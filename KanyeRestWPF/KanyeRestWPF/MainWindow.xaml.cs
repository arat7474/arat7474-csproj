﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KanyeRestWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void GoBtn_Click(object sender, RoutedEventArgs e)
        {
            Song thisSong = new Song();
            string title = TitleGetBox.Text.ToLower();

            thisSong.GetTitle(title);
            thisSong.GetLyrics();
            LyricsOut.Text = thisSong.Lyrics;
        }
    }
}