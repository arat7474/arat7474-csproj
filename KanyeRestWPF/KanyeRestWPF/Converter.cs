﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KanyeRestWPF
{
    class Converter
    {
        public Song Convert(JSonInput input)
        {
            Song theSong = JsonConvert.DeserializeObject<Song>(input.JSon);
            return theSong;
        }
    }
}