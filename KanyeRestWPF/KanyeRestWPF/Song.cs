﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

namespace KanyeRestWPF
{
    class Song
    {
        public string Title { get; set; }
        private string Album { get; set; }
        public string Lyrics { get; set; }
        public void GetTitle(string title)
        {
            title = title.ToLower();
            title = title.Replace(" ", "_");
            this.Title = title;
        }
        public void GetLyrics()
        {
            string track = this.Title;
            string url = @"http://kanyerest.xyz/api/track/" + track;
            WebClient client = new WebClient();
            try
            {
                String DownLyrics = client.DownloadString(url);
                JSonInput inpObj = new JSonInput();
                inpObj.JSon = DownLyrics;
                Converter convertObj = new Converter();
                Song yeah = convertObj.Convert(inpObj);
                this.Lyrics = yeah.Lyrics;
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                {
                    if ((ex.Response as HttpWebResponse).StatusCode == HttpStatusCode.NotFound)
                        this.Lyrics = "Lyrics not found.";
                }
            }
        }

    }
}